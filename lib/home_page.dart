import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class Product {
  final String name;
  final double price;

  Product({required this.name, required this.price});
}

final products = [
  Product(name: "Bluetooth Mouse", price: 2000),
  Product(name: "KeyBoard", price: 1500),
  Product(name: "Headphone", price: 1200),
  Product(name: "iPad", price: 1000),
];

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  int _currentProduct = 0;
  final TextEditingController _controller = TextEditingController();
  bool _check = false;

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.deepPurple,
          title: const Text(
            'Guest price',
            style: TextStyle(fontSize: 40, fontWeight: FontWeight.bold),
          ),
        ),
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 20),
                child: Text(
                  products[_currentProduct].name,
                  style: const TextStyle(
                      fontSize: 40, fontWeight: FontWeight.bold),
                ),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 20),
                child: TextField(
                  controller: _controller,
                  keyboardType:
                      const TextInputType.numberWithOptions(decimal: true),
                  inputFormatters: [FilteringTextInputFormatter.digitsOnly],
                  decoration: const InputDecoration(
                    hintText: 'Enter price',
                  ),
                ),
              ),
              const SizedBox(
                height: 20,
              ),
              ElevatedButton(
                  onPressed: () {
                    String _inputValue = _controller.text;
                    if (double.parse(_inputValue) ==
                        products[_currentProduct].price) {
                      _dialogNotify(context, "Success");
                      setState(() {
                        _check = true;
                      });
                    } else {
                      _dialogNotify(context, "Fail");
                    }
                  },
                  child: const Text('Check')),
              const SizedBox(
                height: 20,
              ),
              Visibility(
                visible: _check,
                child: ElevatedButton(
                    onPressed: () {
                      setState(() {
                        _currentProduct++;
                        if (_currentProduct == products.length) {
                          _currentProduct = 0;
                        }
                        _check = false;
                      });
                      _controller.clear();
                    },
                    child: const Text('Next')),
              )
            ],
          ),
        ),
        floatingActionButton: FloatingActionButton.small(
          onPressed: () {},
          child: const Icon(Icons.add),
        ),
      ),
    );
  }
}

Future<void> _dialogNotify(BuildContext context, String alert) {
  return showDialog(
    context: context,
    builder: (BuildContext context) {
      return AlertDialog(
        content: Center(
            child: Text(
          alert,
          style: const TextStyle(fontSize: 50, fontWeight: FontWeight.bold),
        )),
        actions: [
          TextButton(
            style: TextButton.styleFrom(
              textStyle: Theme.of(context).textTheme.labelLarge,
            ),
            child: const Text('Close'),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
        ],
      );
    },
  );
}
